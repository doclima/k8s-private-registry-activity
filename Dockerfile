FROM php:8.2-fpm-alpine3.18

# install necessary alpine packages
RUN apk update && apk add --no-cache \
    zip \
    curl \
    nginx \
    unzip \
    libpng-dev \
    libzip-dev \
    libxml2-dev  \
    oniguruma 

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql exif pcntl bcmath gd

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

COPY . /var/www/laravel-app

WORKDIR /var/www/laravel-app

RUN composer install && chown -R www-data:www-data /var/www/laravel-app \
    && chmod -R 775 /var/www/laravel-app/storage /var/www/laravel-app/bootstrap/cache

COPY ./nginx_config.conf /etc/nginx/http.d/laravel_config.conf

EXPOSE 8000

CMD php-fpm -D;nginx -g "daemon off;";